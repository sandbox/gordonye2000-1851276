var hostname='';

$(function(){
	$('#a_close').click(function(){
		$('#popup_a').hide(600); 
		return false;
	});			
	hostname = location.host;
});

function display_link_domain(nid, obj)
{	
		$('#popup_a').show(600);
		var offset = $(obj).offset();
		var lt = offset.left-420;
		var tp = offset.top-140;
		$('#popup_a').css('left',lt); 	  
		$('#popup_a').css('top',tp);
		$('#nid').val(nid);
}

function link_domain()
{
	var nid = $('#nid').val();
	var action = $('#action').val();
	var did = $('#domain_id').val();
	$('#a_message').html('<img src="http://'+hostname+'/drupal/sites/all/modules/full_functional_multisite/manage_posts/images/loading.gif"  width="25" height="25" />');
	$.getJSON('domains/action', {nid:nid, did:did, action:action}, function(data){
		$('#d'+nid).html(data.domain);
		$('#a_message').html('The post already assigned to '+data.domain);
		setTimeout(function(){$('#popup_a').hide();},3000);
	});
}

function edit_domain(did)
{
	var theme = $('#theme_'+did).text();
	$('#theme_'+did).html('<input type="textfield" id="theme_edit_'+did+'"  value="'+theme+'" /> ');
	$('#domain_action_'+did).html('<button onclick="update_domain('+did+');" >Update</button> ');
}

function update_domain(did)
{
	var theme = $('#theme_edit_'+did).val();
	$.get('domains/action', {did:did, theme:theme, action:'update_domain'}, function(data){
		$('#theme_'+did).text(theme);
		$('#domain_action_'+did).html('<a onclick="edit_domain('+did+'); return false;" href="#"><img onclick="display_link_domain('+did+', this); return false;" src="http://'+hostname+'/drupal/sites/all/modules/full_functional_multisite/domains/images/edit.jpg"></a>');
	});
}
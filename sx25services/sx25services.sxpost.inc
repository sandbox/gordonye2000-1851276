<?php

class sxpost{

  private $imgpath = "../sites/default/files/";
	
  public function __construct() { }
  
  public function getPosts($domain=null, $amount=10, $start=0)
  {
	if(empty($domain))
		$sql = "SELECT n.nid,n.title,n.type,n.status,n.created,d.domain,f.body_value FROM node n JOIN field_data_body f ON (n.nid=f.entity_id) JOIN domain_object db ON (n.nid=db.entity_id) JOIN domain d ON (db.did=d.did) ORDER BY n.nid ASC LIMIT $amount OFFSET $start ";	  
	else
		$sql = "SELECT n.nid,n.title,n.type,n.status,n.created,d.domain,f.body_value FROM node n JOIN field_data_body f ON (n.nid=f.entity_id) JOIN domain_object db ON (n.nid=db.entity_id) JOIN domain d ON (db.did=d.did) WHERE d.domain='$domain' ORDER BY n.nid ASC LIMIT $amount OFFSET $start ";	  
	    
	$result = db_query($sql);
	while($row = $result->fetchAssoc()) {
		$extra = $this->getPostExtraByNodeID($nid);
		$row['image'] = $this->imgpath.$extra['field_photo']['und'][0]['filename'];
		$created = date("Y-m-d H:i:s", $row['created']);
		$row['created'] = $created;
		$data[$row['nid']] = $row;
	} 
	return $data;
  }
  
  public function getPostByID($nid)
  {
	$sql = "SELECT n.nid,n.title,n.type,n.status,n.created,d.domain,f.body_value FROM node n JOIN field_data_body f ON (n.nid=f.entity_id) JOIN domain_object db ON (n.nid=db.entity_id) JOIN domain d ON (db.did=d.did) WHERE n.nid='$nid' ";	  	    
	$result = db_query($sql);
	while($row = $result->fetchAssoc()) {
		$extra = $this->getPostExtraByNodeID($nid);
		$row['image'] = $this->imgpath.$extra['field_photo']['und'][0]['filename'];
		$created = date("Y-m-d H:i:s", $row['created']);
		$row['created'] = $created;
		$data[$row['nid']] = $row;
	} 	
	return $data;
  }
  
  public function getPostExtraByNodeID($nid)
  {
	$sql = "SELECT data FROM cache_field WHERE cid like 'field:node:$nid' LIMIT 1";	  	    
	$result = db_query($sql)->fetchField(); 
	$data = unserialize($result); //echo "<pre><br />"; print_r($data);echo "</pre>";
	return $data;
  }
  

}



Goals
==============
- Create a unified Drupal API for web services to be exposed in a variety of 
  different server formats.  
- Provide a service browser to be able to test methods.
- Allow distribution of API keys for developer access.

Documentation
==============
1. To retrieve post, the request path is 
		yourdomainname/sx25services/post?action=getPosts
		yourdomainname/sx25services/post?action=getPostByNID&node_id=xx
		yourdomainname/sx25services/post?action=getPostByDomain&domain=domainname
		
2. To retrieve comment, the request path is 
		yourdomainname/sx25services/comment?action=getCommentByCommentID&comment_id=xx
		yourdomainname/sx25services/comment?action=getCommentByNodeID&node_id=xx

Installation
============
Install this module in folder sites/all/modules/. To activate it from Admin panel.

var hostname='';

$(function(){
	$('#a_close').click(function(){
		$('#popup_a').hide(600); 
		return false;
	});	
	
	hostname = location.host;
	
});

function display_link_domain(nid, obj)
{	
		$('#popup_a').show(600);
		var offset = $(obj).offset();
		var lt = offset.left-420;
		var tp = offset.top-140;
		$('#popup_a').css('left',lt); 	  
		$('#popup_a').css('top',tp);
		$('#nid').val(nid);
}


function link_domain()
{
	var nid = $('#nid').val();
	var action = $('#action').val();
	var did = $('#domain_id').val();
	$('#a_message').html('<img src="http://'+hostname+'/drupal/sites/all/modules/full_functional_multisite/posts/images/loading.gif"  width="25" height="25" />');
	$.getJSON('posts/action', {nid:nid, did:did, action:action}, function(data){
		$('#d'+nid).html(data.domain);
		$('#a_message').html('The post already assigned to '+data.domain);
		setTimeout(function(){$('#popup_a').hide();},3000);
	});
}

function getPostByDomain(domain)
{
	$('#loading').append('<img src="http://'+hostname+'/drupal/sites/all/modules/full_functional_multisite/posts/images/loading.gif"  width="25" height="25" />');
	window.location.href = "http://"+hostname+"/drupal/admin/posts/"+domain;
}